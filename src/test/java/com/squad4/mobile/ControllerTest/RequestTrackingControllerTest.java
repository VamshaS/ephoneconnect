package com.squad4.mobile.ControllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.squad4.mobile.controller.RequestTrackingController;
import com.squad4.mobile.dto.RequestTracking;
import com.squad4.mobile.service.RequestTrackingService;

@ExtendWith(MockitoExtension.class)
public class RequestTrackingControllerTest {

	@Mock
	private RequestTrackingService requestTrackingService;

	@InjectMocks
	private RequestTrackingController requestTrackingController;

	@Test
	public void testRequest_Success() {
		List<RequestTracking> requestTrackings = new ArrayList<>();
		when(requestTrackingService.gettracking(1L, 0, 10)).thenReturn(requestTrackings);

		ResponseEntity<List<RequestTracking>> responseEntity = requestTrackingController.request(1L, 0, 10);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(requestTrackings, responseEntity.getBody());
	}
}
