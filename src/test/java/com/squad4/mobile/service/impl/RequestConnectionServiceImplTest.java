package com.squad4.mobile.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.squad4.mobile.dto.RequestConnectionDto;
import com.squad4.mobile.entity.Gender;
import com.squad4.mobile.entity.PhoneNumber;
import com.squad4.mobile.entity.RechargePlan;
import com.squad4.mobile.entity.ServiceProvider;
import com.squad4.mobile.entity.Status;
import com.squad4.mobile.exception.InvalidPlanIdException;
import com.squad4.mobile.repository.ConnectionRequestRepository;
import com.squad4.mobile.repository.PhoneNumberRepository;
import com.squad4.mobile.repository.RechargePlanRepository;
import com.squad4.mobile.repository.ServiceProviderRepository;
import com.squad4.mobile.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
class RequestConnectionServiceImplTest {
	@Mock
	private UserRepository userRepository;
	@Mock
	private RechargePlanRepository rechargePlanRepository;
	@Mock
	private ServiceProviderRepository serviceProviderRepository;
	@Mock
	private PhoneNumberRepository phoneNumberRepository;
	@Mock
	private ConnectionRequestRepository connectionRequestRepository;
	@InjectMocks
	private RequestConnectionServiceImpl requestConnectionServiceImpl;

	@Test
	void testRequestNewConnections() {
		Gender gender = Gender.MALE;
		RequestConnectionDto requestConnectionDto = RequestConnectionDto.builder().email("yamini").phoneNumber("72")
				.planId(1l).ServiceProviderId(1l).build();
		ServiceProvider serviceProvider = ServiceProvider.builder().serviceProviderId(1l).build();
		PhoneNumber phoneNumber = PhoneNumber.builder().contact("72").serviceProvider(serviceProvider)
				.status(Status.AVAILABLE).build();
		RechargePlan rechargePlan = RechargePlan.builder().reachargePlanId(1l).serviceProvider(serviceProvider).build();
		when(rechargePlanRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(InvalidPlanIdException.class,
				() -> requestConnectionServiceImpl.requestNewConnections(gender, requestConnectionDto));
		try {
			requestConnectionServiceImpl.requestNewConnections(gender, requestConnectionDto);
		} catch (InvalidPlanIdException e) {
			assertEquals("EX001", e.getMessage());
		}
	}

}
