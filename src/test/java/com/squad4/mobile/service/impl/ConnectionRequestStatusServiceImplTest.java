package com.squad4.mobile.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.Connection;
import com.squad4.mobile.entity.ConnectionRequest;
import com.squad4.mobile.entity.ConnectionStatus;
import com.squad4.mobile.entity.PhoneNumber;
import com.squad4.mobile.entity.RechargePlan;
import com.squad4.mobile.entity.User;
import com.squad4.mobile.kafkasender.KafkaSender;
import com.squad4.mobile.repository.ConnectionRequestRepository;

@ExtendWith(SpringExtension.class)
class ConnectionRequestStatusServiceImplTest {
	@Mock
	ConnectionRequestRepository connectionRequestRepository;
	@Mock
    KafkaSender kafkaSender;
	@InjectMocks
	ConnectionRequestStatusServiceImpl connectionRequestStatusServiceImpl;
    @Test
    void testSuccess() {
    	Connection status=Connection.REFERED_BACK;
    	Long id=1L;
    	String comment="Not Valid";
    	User user=User.builder().userId(1L).email("vamshas@gmail.com").build();
    	RechargePlan rechargePlan=RechargePlan.builder().reachargePlanId(1L).build();
    	PhoneNumber phoneNumber=PhoneNumber.builder().contact("7483060926").build(); 	
    	ConnectionRequest connectionRequest=ConnectionRequest.builder().connectionStatus(ConnectionStatus.INPROGRESS).user(user).phoneNumber(phoneNumber).build();   	
    	Mockito.when(connectionRequestRepository.findById(1L)).thenReturn(Optional.of(connectionRequest));
    	ApiResponse response=connectionRequestStatusServiceImpl.activateMobileNumber(id,status, comment);  	
    	assertNotNull(response);
    }
}
