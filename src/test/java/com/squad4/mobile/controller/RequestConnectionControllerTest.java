package com.squad4.mobile.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.RequestConnectionDto;
import com.squad4.mobile.entity.Gender;
import com.squad4.mobile.service.RequestConnectionService;
import com.squad4.mobile.util.CustomCode;

@ExtendWith(SpringExtension.class)
class RequestConnectionControllerTest {
	@Mock
	private RequestConnectionService requestConnectionService;
	@InjectMocks
	private RequestConnectionController requestConnectionController;

	@Test
	void testRequestNewConnection() {
		Gender gender = Gender.MALE;
		RequestConnectionDto requestConnectionDto =	RequestConnectionDto.builder().age(18).build();
		ApiResponse api = ApiResponse.builder().code(CustomCode.REQUEST_SUCCESSFULLY_SUBMITTED_CODE)
				.message(CustomCode.REQUEST_SUCCESSFULLY_SUBMITTED_MESSAGE).build();
		when(requestConnectionService.requestNewConnections(gender, requestConnectionDto)).thenReturn(api);
		ResponseEntity<ApiResponse> responseEntity = requestConnectionController.requestNewConnection(gender, requestConnectionDto);
		assertEquals(HttpStatus.CREATED,responseEntity.getStatusCode());
	}

}
