package com.squad4.mobile.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.Connection;
import com.squad4.mobile.service.ConnectionRequestStatusService;

@ExtendWith(SpringExtension.class)
class ConnectionRequestStatusControllerTest {
	@Mock
	ConnectionRequestStatusService connectionRequestStatusService;
	@InjectMocks
	ConnectionRequestStatusController connectionRequestStatusController;
	@Test
	void testSuccess() {
		Long customerId=1L;
		Connection connection=Connection.CONNECTION_ENABLE;
		String comment="Hello";
		Mockito.when(connectionRequestStatusService.activateMobileNumber(customerId, connection, comment)).thenReturn(null);
		ResponseEntity<ApiResponse> responseEntity=connectionRequestStatusController.updateStatus(customerId,connection,comment);
		assertNotNull(responseEntity);
	}
	
}
