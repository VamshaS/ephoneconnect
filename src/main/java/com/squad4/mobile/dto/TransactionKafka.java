package com.squad4.mobile.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionKafka
{
	private String email;
	private String connectionStatus;
	private String comments;
	
}
