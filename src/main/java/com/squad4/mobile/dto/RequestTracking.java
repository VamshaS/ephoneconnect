package com.squad4.mobile.dto;

import com.squad4.mobile.entity.ConnectionStatus;
import com.squad4.mobile.entity.PhoneNumber;

public record RequestTracking(PhoneNumber phoneNumber,

		ConnectionStatus connectionStatus, String comments) {

}
