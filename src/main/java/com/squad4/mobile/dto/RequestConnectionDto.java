package com.squad4.mobile.dto;

import lombok.Builder;

@Builder
public record RequestConnectionDto(String name, String email, Integer age, Long planId, Long ServiceProviderId,
		String phoneNumber) {

}
