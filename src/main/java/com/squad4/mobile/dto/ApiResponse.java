package com.squad4.mobile.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message, String code) {

}
