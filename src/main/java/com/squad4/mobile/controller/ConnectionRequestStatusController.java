package com.squad4.mobile.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.Connection;
import com.squad4.mobile.service.ConnectionRequestStatusService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class ConnectionRequestStatusController {
	
	private final ConnectionRequestStatusService connectionRequestStatusService;
	/**
	 * Activate Mobile Number
	 * @param requestId          request id from user
	 * @param connectionStatus   status of connection
	 * @param comment            comments on status
	 * @return ApiResponse       success message and code
	 */
	@PutMapping("/users/requests/{requestId}")
	public ResponseEntity<ApiResponse> updateStatus(@PathVariable("requestId") Long requestId, @RequestParam Connection connectionStatus,@RequestParam String comment){
		return new ResponseEntity<>(connectionRequestStatusService.activateMobileNumber(requestId, connectionStatus, comment),HttpStatus.OK);
	}
}
