package com.squad4.mobile.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.RequestConnectionDto;
import com.squad4.mobile.entity.Gender;
import com.squad4.mobile.service.RequestConnectionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class RequestConnectionController {

	private final RequestConnectionService requestConnectionService;

	/**
	 * 
	 * @param gender               it will give gender type
	 * @param requestConnectionDto it will information for to request new mobile
	 *                             connection
	 * @return successful message
	 */
	@PostMapping("/user/requests")
	ResponseEntity<ApiResponse> requestNewConnection(@RequestParam Gender gender,
			@RequestBody RequestConnectionDto requestConnectionDto) {
		log.info("user successfully regidterd");
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(requestConnectionService.requestNewConnections(gender, requestConnectionDto));
	}
}
