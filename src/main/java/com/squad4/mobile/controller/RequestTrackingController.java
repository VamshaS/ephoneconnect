package com.squad4.mobile.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.mobile.dto.RequestTracking;
import com.squad4.mobile.service.RequestTrackingService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class RequestTrackingController {

	private final RequestTrackingService requestTrackingService;

	@GetMapping("/RequestTracking/{userid}")
	public ResponseEntity<List<RequestTracking>> request(@PathVariable @Valid Long userid,
			@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
		log.info("ConnectionRequest Fetched Successfully");
		return ResponseEntity.status(HttpStatus.OK)
				.body(requestTrackingService.gettracking(userid, pageNumber, pageSize));
	}
}
