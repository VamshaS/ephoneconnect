package com.squad4.mobile.repository;

<<<<<<< src/main/java/com/squad4/mobile/repository/ConnectionRequestRepository.java
import java.util.List;

import org.springframework.data.domain.Pageable;
=======
import java.util.Optional;

>>>>>>> src/main/java/com/squad4/mobile/repository/ConnectionRequestRepository.java
import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.mobile.entity.ConnectionRequest;
import com.squad4.mobile.entity.User;

public interface ConnectionRequestRepository extends JpaRepository<ConnectionRequest, Long> {

	List<ConnectionRequest> findByUser(User user, Pageable pageable);
	Optional<ConnectionRequest> findByUser(User user);

}
/* List<Transaction> findByCustomer(Customer customer,Pageable pageable); */