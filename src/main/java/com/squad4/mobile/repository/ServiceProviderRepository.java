package com.squad4.mobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.mobile.entity.ServiceProvider;

public interface ServiceProviderRepository extends JpaRepository<ServiceProvider, Long> {

}
