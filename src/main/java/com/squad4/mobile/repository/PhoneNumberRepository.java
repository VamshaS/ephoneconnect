package com.squad4.mobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.mobile.entity.PhoneNumber;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, String> {

}
