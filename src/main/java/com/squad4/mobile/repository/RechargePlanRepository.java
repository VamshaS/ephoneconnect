package com.squad4.mobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.mobile.entity.RechargePlan;

public interface RechargePlanRepository extends JpaRepository<RechargePlan, Long> {

}
