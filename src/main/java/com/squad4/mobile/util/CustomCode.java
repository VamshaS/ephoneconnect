package com.squad4.mobile.util;

public class CustomCode {

	/**
	 * Successful Code And Message
	 */

	public static final String REQUEST_SUCCESSFULLY_SUBMITTED_MESSAGE = "Request succcessfully submitted";
	public static final String REQUEST_SUCCESSFULLY_SUBMITTED_CODE = "SUCCESS001";
	public static final String REQUEST_TRACKED_SUCCESSFULLY_MESSAGE = "Request tracked succcessfully submitted";
	public static final String REQUEST_TRACKED_SUCCESSFULLY_CODE = "SUCCESS002";
	public static final String CONNECTION_STATUS_UPDATED_SUCCESSFULLY_MESSAGE = "connection status updated successfully";
	public static final String CONNECTION_STATUS_UPDATED_SUCCESSFULLY_CODE = "SUCCESS003";

	/**
	 * Exception Code
	 */
	public static final String INVALID_PLAN_ID_EXCEPTION_MESSAGE = "invalid plane id exception";
	public static final String INVALID_PLAN_ID_EXCEPTION_CODE = "EX001";
	public static final String INVALID_SERVICE_PROVIDER_EXCEPTION_MESSAGE = "invalid service provider exception";
	public static final String INVALID_SERVICE_PROVIDER_EXCEPTION_CODE = "EX002";
	public static final String INVALID_CONTACT_EXCEPTION_MESSAGE = "invalid contact exception";
	public static final String INVALID_CONTACT_EXCEPTION_CODE = "EX003";
	public static final String MISMATCHED_RESOURCE_EXCEPTION_MESSAGE = "mismatched resource exception";
	public static final String MISMATCHED_RESOURCE_EXCEPTION_CODE = "EX004";
	public static final String PHONE_NOT_AVAILABLE_EXCEPTION_MESSAGE = "phone number not availble exception";
	public static final String PHONE_NOT_AVAILABLE_EXCEPTION_CODE = "EX005";
	public static final String ACTIVE_SUBSCRIBER_EXCEPTION_MESSAGE = "active subscriber exception";
	public static final String ACTIVE_SUBSCRIBER_EXCEPTION_CODE = "EX006";
	public static final String USER_NOT_FOUND_EXCEPTION_MESSAGE = "user not found exception";
	public static final String USER_NOT_FOUND_EXCEPTION_CODE = "EX007";
	public static final String REQUEST_ID_EXCEPTION_MESSAGE = "request id not found exception";
	public static final String REQUESTID_EXCEPTION_CODE = "EX008";
	public static final String REQUEST_ALREADY_APPROVED_EXCEPTION_MESSAGE = "request already approved exception";
	public static final String REQUESTALREADYAPPROVEDEXCEPTIONCODE = "EX009";

	private CustomCode() {
		super();
	}
}
