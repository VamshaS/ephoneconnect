package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class RequestAlreadyUpdatedException  extends GlobalException{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RequestAlreadyUpdatedException() {
		super(CustomCode.REQUEST_ALREADY_APPROVED_EXCEPTION_MESSAGE, CustomCode.REQUESTALREADYAPPROVEDEXCEPTIONCODE);
	}
	

}
