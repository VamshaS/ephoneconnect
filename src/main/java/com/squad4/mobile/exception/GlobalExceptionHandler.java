package com.squad4.mobile.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
	}


	@ExceptionHandler(UserNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleUserNotFoundException(
			UserNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}


	@ExceptionHandler(RequestIdNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleRequestIdNotFoundException(RequestIdNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	@ExceptionHandler(RequestAlreadyUpdatedException.class)
	protected ResponseEntity<ErrorResponse> handleRequestAlreadyUpdatedException(RequestAlreadyUpdatedException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());

	@ExceptionHandler(InvalidPlanIdException.class)
	protected ResponseEntity<ErrorResponse> handleInvalidPlanIdException(
			InvalidPlanIdException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());

	}

	@ExceptionHandler(InvalidServiceProviderException.class)
	protected ResponseEntity<ErrorResponse> handleInvalidServiceProviderException(
			InvalidServiceProviderException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}

	@ExceptionHandler(InvalidContactException.class)
	protected ResponseEntity<ErrorResponse> handleInvalidContactException(
			InvalidContactException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}

	@ExceptionHandler(PhoneNumberNotAvailableException.class)
	protected ResponseEntity<ErrorResponse> PhoneNumberNotAvailableException(
			PhoneNumberNotAvailableException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}

	@ExceptionHandler(MismatchedResourceException.class)
	protected ResponseEntity<ErrorResponse> mismatchedResourceException(
			MismatchedResourceException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.CONFLICT).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}

	@ExceptionHandler(ActiveSubscriberException.class)
	protected ResponseEntity<ErrorResponse> activeActiveSubscriberException(
			ActiveSubscriberException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}
}
