package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class InvalidContactException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidContactException() {
		super(CustomCode.INVALID_CONTACT_EXCEPTION_CODE, CustomCode.INVALID_CONTACT_EXCEPTION_MESSAGE);
	}

}
