package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class MismatchedResourceException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MismatchedResourceException() {
		super(CustomCode.MISMATCHED_RESOURCE_EXCEPTION_CODE, CustomCode.MISMATCHED_RESOURCE_EXCEPTION_MESSAGE);

	}

}
