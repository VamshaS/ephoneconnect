package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class ActiveSubscriberException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ActiveSubscriberException() {
		super(CustomCode.ACTIVE_SUBSCRIBER_EXCEPTION_CODE, CustomCode.ACTIVE_SUBSCRIBER_EXCEPTION_MESSAGE);

	}

}
