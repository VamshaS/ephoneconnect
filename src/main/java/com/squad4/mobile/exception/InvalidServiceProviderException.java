package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class InvalidServiceProviderException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidServiceProviderException() {
		super(CustomCode.INVALID_SERVICE_PROVIDER_EXCEPTION_CODE,
				CustomCode.INVALID_SERVICE_PROVIDER_EXCEPTION_MESSAGE);

	}

}
