package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class InvalidPlanIdException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPlanIdException() {
		super(CustomCode.INVALID_PLAN_ID_EXCEPTION_CODE, CustomCode.INVALID_PLAN_ID_EXCEPTION_MESSAGE);

	}
}
