package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class UserNotFoundException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public UserNotFoundException() {
		super(CustomCode.USER_NOT_FOUND_EXCEPTION_CODE, CustomCode.USER_NOT_FOUND_EXCEPTION_MESSAGE);

	}

}