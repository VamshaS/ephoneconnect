package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class RequestIdNotFoundException  extends GlobalException{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RequestIdNotFoundException() {
		super(CustomCode.REQUEST_ID_EXCEPTION_MESSAGE, CustomCode.REQUESTID_EXCEPTION_CODE);
	}
	

}
