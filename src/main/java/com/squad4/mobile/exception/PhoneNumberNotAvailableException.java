package com.squad4.mobile.exception;

import com.squad4.mobile.util.CustomCode;

public class PhoneNumberNotAvailableException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PhoneNumberNotAvailableException() {
		super(CustomCode.PHONE_NOT_AVAILABLE_EXCEPTION_CODE, CustomCode.PHONE_NOT_AVAILABLE_EXCEPTION_MESSAGE);
	}

}
