package com.squad4.mobile.service;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.RequestConnectionDto;
import com.squad4.mobile.entity.Gender;

public interface RequestConnectionService {

	ApiResponse requestNewConnections(Gender gender, RequestConnectionDto requestConnectionDto);

}
