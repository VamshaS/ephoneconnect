package com.squad4.mobile.service;

import java.util.List;

import com.squad4.mobile.dto.RequestTracking;

public interface RequestTrackingService {

	public List<RequestTracking> gettracking(Long userId, Integer pageNumber, Integer pageSize);
}
