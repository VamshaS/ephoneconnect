package com.squad4.mobile.service;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.Connection;

public interface ConnectionRequestStatusService {
	
	/**
	 * 
	 * @param requestId          request id from user
	 * @param connectionStatus   status of connection
	 * @param comment            comments on status
	 * @return ApiResponse       success message and code
	 */
	ApiResponse activateMobileNumber(Long requestId,Connection connectionStatus,String comment);
}
