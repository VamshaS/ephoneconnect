package com.squad4.mobile.service.impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.RequestConnectionDto;
import com.squad4.mobile.entity.ConnectionRequest;
import com.squad4.mobile.entity.ConnectionStatus;
import com.squad4.mobile.entity.Gender;
import com.squad4.mobile.entity.PhoneNumber;
import com.squad4.mobile.entity.RechargePlan;
import com.squad4.mobile.entity.ServiceProvider;
import com.squad4.mobile.entity.Status;
import com.squad4.mobile.entity.User;
import com.squad4.mobile.exception.ActiveSubscriberException;
import com.squad4.mobile.exception.InvalidContactException;
import com.squad4.mobile.exception.InvalidPlanIdException;
import com.squad4.mobile.exception.InvalidServiceProviderException;
import com.squad4.mobile.exception.MismatchedResourceException;
import com.squad4.mobile.exception.PhoneNumberNotAvailableException;
import com.squad4.mobile.repository.ConnectionRequestRepository;
import com.squad4.mobile.repository.PhoneNumberRepository;
import com.squad4.mobile.repository.RechargePlanRepository;
import com.squad4.mobile.repository.ServiceProviderRepository;
import com.squad4.mobile.repository.UserRepository;
import com.squad4.mobile.service.RequestConnectionService;
import com.squad4.mobile.util.CustomCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class RequestConnectionServiceImpl implements RequestConnectionService {
	/**
	 * Logic to request newcollection
	 */
	private final UserRepository userRepository;
	private final RechargePlanRepository rechargePlanRepository;
	private final ServiceProviderRepository serviceProviderRepository;
	private final PhoneNumberRepository phoneNumberRepository;
	private final ConnectionRequestRepository connectionRequestRepository;

	@Override
	public ApiResponse requestNewConnections(Gender gender, RequestConnectionDto requestConnectionDto) {
		Optional<User> user = userRepository.findByEmail(requestConnectionDto.email());
		log.warn("InvalidPlanIdException occured");
		RechargePlan recharge = rechargePlanRepository.findById(requestConnectionDto.planId())
				.orElseThrow(InvalidPlanIdException::new);
		log.warn("InvalidServiceProviderException occured");
		ServiceProvider serviceProvider = serviceProviderRepository.findById(requestConnectionDto.ServiceProviderId())
				.orElseThrow(InvalidServiceProviderException::new);
		log.warn("InvalidContactException occured");
		PhoneNumber phoneNumber = phoneNumberRepository.findById(requestConnectionDto.phoneNumber())
				.orElseThrow(InvalidContactException::new);

		if (phoneNumber.getStatus().equals(Status.NOT_AVAILABLE)) {
			log.warn("PhoneNumberNotAvailableException occured");
			;
			throw new PhoneNumberNotAvailableException();
		} else if (phoneNumber.getServiceProvider() != serviceProvider
				|| serviceProvider != recharge.getServiceProvider()) {
			log.warn("MismatchedResourceException occured");
			throw new MismatchedResourceException();
		}

		if (user.isEmpty()) {
			User user1 = User.builder().age(requestConnectionDto.age()).email(requestConnectionDto.email())
					.gender(gender).userName(requestConnectionDto.name()).build();
			userRepository.save(user1);
			phoneNumber.setStatus(Status.NOT_AVAILABLE);
			phoneNumberRepository.save(phoneNumber);
			ConnectionRequest connectionRequest = ConnectionRequest.builder()
					.connectionStatus(ConnectionStatus.INPROGRESS).phoneNumber(phoneNumber).rechargePlan(recharge)
					.user(user1).build();
			connectionRequestRepository.save(connectionRequest);
			log.info("request submitted successfully");
			return ApiResponse.builder().code(CustomCode.REQUEST_SUCCESSFULLY_SUBMITTED_CODE)
					.message(CustomCode.REQUEST_SUCCESSFULLY_SUBMITTED_MESSAGE).build();
		}

		else {
			User user1 = user.get();
			Optional<ConnectionRequest> connectionRequest = connectionRequestRepository.findByUser(user1);
			if (connectionRequest.isPresent()) {
				ConnectionRequest connectionRequest1 = connectionRequest.get();
				if (connectionRequest1.getConnectionStatus().equals(ConnectionStatus.CONNECTION_ENABLE)) {
					throw new ActiveSubscriberException();
				} else {
					phoneNumber.setStatus(Status.NOT_AVAILABLE);
					phoneNumberRepository.save(phoneNumber);
					ConnectionRequest connectionRequest2 = ConnectionRequest.builder()
							.connectionStatus(ConnectionStatus.INPROGRESS).phoneNumber(phoneNumber)
							.rechargePlan(recharge).user(user1).build();
					connectionRequestRepository.save(connectionRequest2);
				}

			}
			log.info("request submitted successfully");
			return ApiResponse.builder().code(CustomCode.REQUEST_SUCCESSFULLY_SUBMITTED_CODE)
					.message(CustomCode.REQUEST_SUCCESSFULLY_SUBMITTED_MESSAGE).build();
		}

	}
}