package com.squad4.mobile.service.impl;

import org.springframework.stereotype.Service;

import com.squad4.mobile.dto.ApiResponse;
import com.squad4.mobile.dto.Connection;
import com.squad4.mobile.entity.ConnectionRequest;
import com.squad4.mobile.entity.ConnectionStatus;
import com.squad4.mobile.exception.RequestAlreadyUpdatedException;
import com.squad4.mobile.exception.RequestIdNotFoundException;
import com.squad4.mobile.kafkasender.KafkaSender;
import com.squad4.mobile.repository.ConnectionRequestRepository;
import com.squad4.mobile.service.ConnectionRequestStatusService;
import com.squad4.mobile.util.CustomCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConnectionRequestStatusServiceImpl  implements ConnectionRequestStatusService{
	
	private final ConnectionRequestRepository connectionRequestRepository;
	private final KafkaSender kafkaSender;
   
	/**
	 * Activate Mobile Number
	 * @param requestId          request id from user
	 * @param connectionStatus   status of connection
	 * @param comment            comments on status
	 * @return ApiResponse       success message and code
	 */
	@Override
	public ApiResponse activateMobileNumber(Long requestId, Connection connectionStatus,String comment) {
		        ConnectionRequest connectionRequest=connectionRequestRepository.findById(requestId).orElseThrow(RequestIdNotFoundException::new);
			    if(connectionRequest.getConnectionStatus().equals(ConnectionStatus.CONNECTION_ENABLE) || connectionRequest.getConnectionStatus().equals(ConnectionStatus.REJECTED)) {
			    	log.warn(CustomCode.REQUEST_ALREADY_APPROVED_EXCEPTION_MESSAGE);
			    	throw new RequestAlreadyUpdatedException();
			    }
			    connectionRequest.setConnectionStatus(ConnectionStatus.valueOf(connectionStatus.toString()));
			    connectionRequest.setComments(comment);
			    connectionRequestRepository.save(connectionRequest);
//			    kafkaSender.send(new TransactionKafka(connectionRequest.getUser().getEmail(), connectionStatus.toString(), comment));
			    log.info(CustomCode.CONNECTION_STATUS_UPDATED_SUCCESSFULLY_MESSAGE);
		        return new ApiResponse(CustomCode.CONNECTION_STATUS_UPDATED_SUCCESSFULLY_MESSAGE, CustomCode.CONNECTION_STATUS_UPDATED_SUCCESSFULLY_CODE);
	}
	

}
