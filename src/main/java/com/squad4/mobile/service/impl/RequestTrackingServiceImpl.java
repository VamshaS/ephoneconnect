package com.squad4.mobile.service.impl;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.squad4.mobile.dto.RequestTracking;
import com.squad4.mobile.entity.ConnectionRequest;
import com.squad4.mobile.entity.User;
import com.squad4.mobile.exception.UserNotFoundException;
import com.squad4.mobile.repository.ConnectionRequestRepository;
import com.squad4.mobile.repository.UserRepository;
import com.squad4.mobile.service.RequestTrackingService;
import com.squad4.mobile.util.CustomCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class RequestTrackingServiceImpl implements RequestTrackingService {
	private final UserRepository userRepository;
	private final ConnectionRequestRepository connectionRequestRepository;

	@Override
	public List<RequestTracking> gettracking(Long userId, @RequestParam Integer pageNumber,
			@RequestParam Integer pageSize) {
		User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		List<ConnectionRequest> connectionRequest = connectionRequestRepository.findByUser(user, pageable);
		if (connectionRequest.isEmpty()) {
			log.warn(CustomCode.USER_NOT_FOUND_EXCEPTION_MESSAGE);
			throw new UserNotFoundException();
		}
		return connectionRequest.stream().map(connection -> {

			return new RequestTracking(connection.getPhoneNumber(), connection.getConnectionStatus(),
					connection.getComments());
		}).toList();
	}
}
