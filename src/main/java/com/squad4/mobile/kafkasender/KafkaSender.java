package com.squad4.mobile.kafkasender;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.squad4.mobile.dto.TransactionKafka;
import com.squad4.mobile.serializer.JsonSerializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KafkaSender {

	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(TransactionKafka transactionKafka) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, TransactionKafka> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),


				new JsonSerializer());	
		
		log.info("Message Sent: "+transactionKafka);
		ProducerRecord<String, TransactionKafka> record = new ProducerRecord<String, TransactionKafka>("notifier-queue", transactionKafka);
		log.info(""+record);
		

				new JsonSerializer());

		log.info("Message Sent: " + transactionKafka);
		ProducerRecord<String, TransactionKafka> record = new ProducerRecord<String, TransactionKafka>("exception",
				transactionKafka);
		log.info("" + record);

>>>>>>> src/main/java/com/squad4/mobile/kafkasender/KafkaSender.java

	}

}
