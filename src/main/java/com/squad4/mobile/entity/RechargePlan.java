package com.squad4.mobile.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RechargePlan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long reachargePlanId;
	@Enumerated(EnumType.STRING)
	private PlanName planName;
	private Double price;
	private Integer validityDate;
	private String dataLimit;
	@ManyToOne(cascade = CascadeType.ALL)
	private ServiceProvider serviceProvider;
}
